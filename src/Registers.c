#include "Registers.h"

void Initialize(void){
	SYSCON_SYSAHBCLKCTRL0_ADDR->GPIO1=1;
	SYSCON_SYSAHBCLKCTRL0_ADDR->GPIO0=1;
}

void SetDir(GPIO_t PORT, unsigned int PIN, DIR_t DIR){
	switch(PORT){
			case PORT_GPIO0:
				if(PIN<=31) {
					if(DIR == INPUT){
						GPIO_DIR0_ADDR &=~(1<<PIN);
					}else if(DIR == OUTPUT){
						GPIO_DIR0_ADDR |=(1<<PIN);
					}
				}
				break;
			case PORT_GPIO1:
				if(PIN<=21){
					if(DIR == INPUT){
						GPIO_DIR1_ADDR &=~(1<<PIN);
					}else if(DIR == OUTPUT){
						GPIO_DIR1_ADDR |=(1<<PIN);
					}
				}
				break;
			default:
				break;
		}
}

void SetPin(GPIO_t PORT,unsigned int PIN){
	switch(PORT){
		case PORT_GPIO0:
			if(PIN<=31)	GPIO_SET0_ADDR |=(1<<PIN);
			break;
		case PORT_GPIO1:
			if(PIN<=21) GPIO_SET1_ADDR |=(1<<PIN);
			break;
		default:
			break;
	}
}
void ClearPin(GPIO_t PORT,unsigned int PIN){
	switch(PORT){
		case PORT_GPIO0:
			if(PIN<=31) GPIO_CLR0_ADDR |=(1<<PIN);
			break;
		case PORT_GPIO1:
			if(PIN<=21) GPIO_CLR1_ADDR |=(1<<PIN);
			break;
		default:
			break;
	}
}

void SetNibble(GPIO_t PORT, unsigned int PIN, unsigned char value){
	if(value<=0xF){
		switch(PORT){
			case PORT_GPIO0:
				if(PIN<=28){
					GPIO_CLR0_ADDR |= (1<<PIN)|(1<<(PIN+1))|(1<<(PIN+2))|(1<<(PIN+3));
					GPIO_SET0_ADDR |= (value<<PIN);
				}
				break;
			case PORT_GPIO1:
				if(PIN<=18){
					GPIO_CLR1_ADDR |= (1<<PIN)|(1<<(PIN+1))|(1<<(PIN+2))|(1<<(PIN+3));
					GPIO_SET1_ADDR |= (value<<PIN);
				}
				break;
		}
	}
}


