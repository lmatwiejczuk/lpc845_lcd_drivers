#include "Registers.h"
#include "Lcd.h"

static void delay(){
	int i=0;
	for(i=0;i<10000;i++){
	}
}

void LCD_INIT(void){
	Initialize();
	LCD_PIN_INIT();
	LCD_CMD(LCD_CMD_4BIT_MODE);
	LCD_CMD(LCD_CMD_2LINE_5x8_MODE);
	LCD_CMD(LCD_CMD_FORCE_CURSOR_1ST_LINE);
	LCD_CMD(LCD_CMD_DISPLAY_ON_NO_CURSOR);
}

void LCD_CMD(unsigned char cmd){
	static unsigned char temp=0;
	ClearPin(LCD_PORT, LCD_RS);	//Set Rs to 0 (cmd mode)
	temp = cmd & 0xF0;
	SetNibble(LCD_PORT, LCD_DATABITS_START_BIT, (temp>>4));	//send higher nibble
	SetPin(LCD_PORT, LCD_ENABLE);	//Set Enable to 1
	delay();
	ClearPin(LCD_PORT, LCD_ENABLE);	//Set Enable to 0
	temp = cmd & 0x0F;
	SetNibble(LCD_PORT, LCD_DATABITS_START_BIT, temp);	//send lower nibble
	SetPin(LCD_PORT, LCD_ENABLE);	//Set Enable to 1
	delay();
	ClearPin(LCD_PORT, LCD_ENABLE);	//Set Enable to 0
}
void LCD_CHAR(unsigned char msg){
	static unsigned char temp=0;
	SetPin(LCD_PORT, LCD_RS);	//Set Rs to 1 (data mode)
	temp = msg & 0xF0;
	SetNibble(LCD_PORT, LCD_DATABITS_START_BIT, (temp>>4));	//send higher nibble
	SetPin(LCD_PORT, LCD_ENABLE);	//Set Enable to 1
	delay();
	ClearPin(LCD_PORT, LCD_ENABLE);	//Set Enable to 0
	temp = msg & 0x0F;
	SetNibble(LCD_PORT, LCD_DATABITS_START_BIT, temp);	//send lower nibble
	SetPin(LCD_PORT, LCD_ENABLE);	//Set Enable to 1
	delay();
	ClearPin(LCD_PORT, LCD_ENABLE);	//Set Enable to 0

}
void LCD_STRING(const char *msg){
	while((*msg)!=0){
		LCD_CHAR(*msg);
		msg++;
	}
}

void LCD_CLR(void){
	LCD_CMD(LCD_CMD_CLEAR_SCREEN);
	delay();
}

void LCD_PIN_INIT(void){
	ClearPin(LCD_PORT, LCD_ENABLE);	//Set Enable to 0
	ClearPin(LCD_PORT, LCD_RS);	//Set Rs to 0
	ClearPin(LCD_PORT, LCD_DATABITS_B4);	//Set databit4 to 0
	ClearPin(LCD_PORT, LCD_DATABITS_B5);	//Set databit5 to 0
	ClearPin(LCD_PORT, LCD_DATABITS_B6);	//Set databit6 to 0
	ClearPin(LCD_PORT, LCD_DATABITS_B7);	//Set databit7 to 0
	SetDir(LCD_PORT, LCD_ENABLE, OUTPUT);
	SetDir(LCD_PORT, LCD_RS, OUTPUT);
	SetDir(LCD_PORT, LCD_DATABITS_B7, OUTPUT);
	SetDir(LCD_PORT, LCD_DATABITS_B6, OUTPUT);
	SetDir(LCD_PORT, LCD_DATABITS_B5, OUTPUT);
	SetDir(LCD_PORT, LCD_DATABITS_B4, OUTPUT);

	SetDir(PORT_GPIO1, GPIO1_2, OUTPUT);
}
