#ifndef LCD_H_
#define LCD_H_

#define LCD_PORT PORT_GPIO0

#define LCD_RS GPIO0_21
#define LCD_ENABLE GPIO0_22
#define LCD_DATABITS_B7 GPIO0_19
#define LCD_DATABITS_B6 GPIO0_18
#define LCD_DATABITS_B5 GPIO0_17
#define LCD_DATABITS_B4 GPIO0_16

#define LCD_DATABITS_START_BIT LCD_DATABITS_B4	//Start bit set on GPIO0_16

#define LCD_CMD_4BIT_MODE 0b00100000
#define LCD_CMD_2LINE_5x8_MODE 0b00101000
#define LCD_CMD_CLEAR_SCREEN 0b00000001
#define LCD_CMD_DISPLAY_ON_NO_CURSOR 0b00001110
#define LCD_CMD_FORCE_CURSOR_1ST_LINE 0b00010000

void LCD_PIN_INIT(void);
void LCD_INIT(void);
void LCD_CMD(unsigned char cmd);
void LCD_CHAR(unsigned char msg);
void LCD_STRING(const char *msg);
void LCD_CLR(void);

#endif /* LCD_H_ */
